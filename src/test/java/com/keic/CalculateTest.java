package com.keic;

import static org.hamcrest.CoreMatchers.is;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ErrorCollector;

public class CalculateTest {

  String [] cases = new String[18]; // cases
  double [] calcExpected = new double[18]; // expected calculated results
  boolean [] bracketsExpected = new boolean[18]; // brackets expected
  boolean [] tokensExpected = new boolean[18]; // adj tokens expected
  
  @Rule
  public ErrorCollector collector = new ErrorCollector();

  @Before
  public void setUp() throws Exception {
    cases[0] = "1 + 1";
    cases[1] = "2 * 2";
    cases[2] = "1 + 2 + 3";
    cases[3] = "6 / 2";
    cases[4] = "11 + 23";
    cases[5] = "11.1 + 23";
    cases[6] = "1 + 1 * 3";
    cases[7] = "( 11.5 + 15.4 ) + 10.1";
    cases[8] = "23 - ( 29.3 - 12.5 )";
    cases[9] = "10 - ( 2 + 3 * ( 7 - 5 ) )";
    cases[10] = "2 / 3 - ( - 6 )";
    cases[11] = "( 2.2 * (  2 / 3 + 7 ) ) - ( 2 - 3 )";
    cases[12] = "2.2 / (  2 / 3 / 4 + 7 ) - ( 2 * ( 4.4 + 1.1 * ( - 3.3 - 2 ) ) - 3 )";
    cases[13] = " 4.4 + 1.1 * ( - 3.3 - 2 ) ) - ";
    cases[14] = "- 3 + ()";
    cases[15] = "( )";
    cases[16] = "( ( 5 + ) )";
    cases[17] = "4 - ( - 6 - 7.1 )";
    
    calcExpected[0] = 2.0;
    calcExpected[1] = 4.0;
    calcExpected[2] = 6.0;
    calcExpected[3] = 3.0;
    calcExpected[4] = 34.0;
    calcExpected[5] = 34.1;
    calcExpected[6] = 4.0;
    calcExpected[7] = 37.0;
    calcExpected[8] = 6.2;
    calcExpected[9] = 2.0;
    calcExpected[10] = 6.666667;
    calcExpected[11] = 17.866667;
    calcExpected[12] = 6.166977;
    
    bracketsExpected[0] = true;
    bracketsExpected[1] = true;
    bracketsExpected[2] = true;
    bracketsExpected[3] = true;
    bracketsExpected[4] = true;
    bracketsExpected[5] = true;
    bracketsExpected[6] = true;
    bracketsExpected[7] = true;
    bracketsExpected[8] = true;
    bracketsExpected[9] = true;
    bracketsExpected[10] = true;
    bracketsExpected[11] = true;
    bracketsExpected[12] = true;
    bracketsExpected[13] = false;
    bracketsExpected[14] = true;
    bracketsExpected[15] = true;
    bracketsExpected[16] = true;
    bracketsExpected[17] = true;
    
    tokensExpected[0] = true;
    tokensExpected[1] = true;
    tokensExpected[2] = true;
    tokensExpected[3] = true;
    tokensExpected[4] = true;
    tokensExpected[5] = true;
    tokensExpected[6] = true;
    tokensExpected[7] = true;
    tokensExpected[8] = true;
    tokensExpected[9] = true;
    tokensExpected[10] = true;
    tokensExpected[11] = true;
    tokensExpected[12] = true;
    tokensExpected[13] = false;
    tokensExpected[14] = false;
    tokensExpected[15] = false;
    tokensExpected[16] = false;
    tokensExpected[17] = true;
    
  }
  
  @Test
  public void testCalculate() {
    for (int i=0; i<13; i++) {
      String caseN = cases[i];
      double expected = calcExpected[i];
      try {
        collector.checkThat("Fail calculate test for " + caseN, Calculate.calculate(caseN), Matchers.closeTo(expected, 0.000001));
      } catch (Exception e) {
        System.out.println(caseN + ":" + e.getMessage());
      } finally {
        try {
          System.out.println("Test case: " + caseN + " = " + Calculate.calculate(caseN));
        } catch (Exception e) {
          
        }
      }
    }
  }
  
  @Test
  public void testValidateChars() {
    for (int i=0; i<cases.length; i++) {
      String caseN = cases[i];
      try {
        collector.checkThat("Test for valid chars: " + caseN, Calculate.validateChars(caseN), is(true));
      } catch (Exception e) {
        System.out.println(caseN + ": " + e.getMessage());
      } finally {
      }
    }
  }
  
  @Test
  public void testValidateBrackets() {
    for (int i=0; i<cases.length; i++) {
      String caseN = cases[i];
      try {
        collector.checkThat("Fail backets check for " + caseN, Calculate.validateBrackets(caseN), is(bracketsExpected[i]));
      } catch (Exception e) {
        System.out.println(caseN + ": " + e.getMessage());
      } finally {
        System.out.println("Test brackets: " + caseN + " : " + Calculate.validateBrackets(caseN));
      }
    }
  }
  
  @Test
  public void testValidateTokens() {
    for (int i=0; i<cases.length; i++) {
      String caseN = cases[i];
      try {
        collector.checkThat("Fail tokens check for " + caseN, Calculate.validateTokens(caseN), is(tokensExpected[i]));
      } catch (Exception e) {
        System.out.println(caseN + ": " + e.getMessage());
      } finally {
        System.out.println("Test tokens: " + caseN + " : " + Calculate.validateTokens(caseN));
      }
    }
  }
  
}
