package com.keic;

import java.io.Console;
import java.util.regex.Pattern;

public class Calculate {
  
  /**
   * This method takes in an arithmetic expression in text format. It evaluates
   * and performs calculation on it. The result is returned as a <tt>double</tt>.<br>
   * Example: <br>
   * Input: - 5 + 3.1 * ( 10 - 2 ) <br>
   * Output: 19.8 <br>
   * 
   * @param sum
   * @return
   * @throws Exception
   */
  public static double calculate(String sum) throws Exception {
    double result = 0;
    try {
      boolean rule1 = validateChars(sum);
      boolean rule2 = validateTokens(sum);
      boolean rule3 = validateBrackets(sum);
      if (!rule1||!rule2||!rule3) {
        throw new Exception("Ensure valid characters are used and brackets are balanced if any.");
      }
      String value = solveBracketedExpression(sum);
      result = Double.parseDouble(value);
    } catch (Exception e) {
      throw e;
    }
    return result;
  }
  
  /**
   * This method takes in a simple arithmetic expression without brackets. It
   * evaluates and performs calculation on it, complying to PEDMAS rules where
   * multiplication and division take precedence of addition and subtraction.<p>
   * 
   * The operators and operands must be separated by spaces. e.g. 3 + 4.1 * 6 / 5 <p>
   * 
   * Negation or negative numbers can be with or without spaces between the
   * negative sign and number. e.g. -3 - 4 = -3 -4 <p>
   * 
   * Note:
   * This method does not handle other operators other than multiplication,
   * division, addition, subtraction, and negation
   * 
   * @param exp
   * @return
   * @throws Exception
   */
  private static double dmas(String exp) throws Exception {
    double total = 0;
    try {
      String[] expArr = exp.split(" ");
      double[] numArr = new double[expArr.length];
      int i = 0;
      int n = 0;
      String op = null;
      double sign = 1;
      double prevV = 1;
      // Handles positive and negative (unary op) numbers and at the same
      // time perform multiplication and division
      // This part converts the arithmetic string expression to
      // arrays of signed numbers for addition in the second part below
      // - 1 + 2 * 3 - 3 / 2
      // => -1, 6, -1.5
      while (i<expArr.length) {
        String c = expArr[i];
        if (c.equals("")) {
        } else if (c.equals("-")) {
          sign = -1;
          op = "as";
        } else if (c.equals("+")) {
          sign = 1;
          op = "as";
        } else if (c.equals("*")) {
          n--;
          sign = prevV;
          op = "m";
        } else if (c.equals("/")) {
          n--;
          sign = prevV;
          op = "d";
        } else {
          double v = Double.parseDouble(c);
          if (op!=null && op.equals("m")) {
            v = sign*v;
          } else if (op!=null && op.equals("d")) {
            v = sign/v;
          } else if (op!=null && op.equals("as")) {
            v = sign*v;
          }
          numArr[n++] = v;
          prevV = v;
        }
        i++;
      }
      // addition
      for (int j=0; j<n; j++) {
        total += numArr[j];
      }
    } catch (Exception e) {
      throw new Exception("Expression is invalid");
    }
    return total;
  }
  
  /**
   * This method takes in an arithmetic expression with nested brackets. It
   * evaluates and performs calculation on it.<p>
   * 
   * @param exp
   * @return
   * @throws Exception
   */
  private static String solveBracketedExpression(String exp) throws Exception {
    String res = null;
    try {
      int[] imb = innerMatchingBrackets(exp);
      String s = null;
      if (imb!=null) {
        // solve the expression within the innermost brackets
        String bCtn = exp.substring(imb[0]+1, imb[1]);
        String bCtnVal = String.valueOf(dmas(bCtn));
        String pre = exp.substring(0, imb[0]);
        String post = exp.substring(imb[1]+1, exp.length());
        String newExp = pre + bCtnVal + post;
        // Innermost brackets solved, form new expression and solve recursively
        // until there's no brackets found
        s = solveBracketedExpression(newExp);
      } else {
        // No brackets found
        s = exp;
      }
      // Solved bracketed expression becomes a simple arithmetic expression
      // that can be evaluated using dmas(String expression) method
      res = String.valueOf(dmas(s));
    } catch (Exception e) {
      throw e;
    }
    return res;
  }

  /**
   * Find innermost matching brackets pair. It searches from left to right and
   * returns the character positions of the first encountered innermost
   * matching brackets in a two-element integer array. It also determines if
   * the brackets are balanced.<p>
   * 
   * element[0] = open bracket position <br>
   * element[1] = matching close bracket position <p>
   * 
   * If no brackets, it returns <tt>&lt;null&gt;</tt><br>
   * 
   * It throws an exception if the brackets are unbalanced or other runtime
   * error.
   * 
   * @param exp
   * @return
   * @throws Exception
   */
  private static int[] innerMatchingBrackets(String exp) throws Exception {
    int [] mb = null;
    try {
      boolean bFound = false;
      int bC = 0; // bracket counter
      int mb1 = -1;
      int mb2 = -1;
      char[] c = exp.toCharArray();
      for (int i=0; i<c.length; i++) {
        if (c[i]=='(') {
          bFound = true;
          bC++;
          if (mb2==-1) mb1 = i; // save open bracket position, effectively this is the first encountered innermost open bracket
        } else if (c[i]==')') {
          bC--;
          if (bC<0) break; // imbalance, e.g. ( ) )
          if (mb2==-1) mb2 = i; // save matching bracket position
        }
      }
      if (bFound) {
        mb = new int[2];
        if (bC==0) {
          // balanced
          mb[0] = mb1;
          mb[1] = mb2;
        } else {
          // imbalanced
          mb[0] = -1;
          mb[1] = -1;
          //throw new Exception("brackets unbalanced");
        }
      }
    } catch (Exception e) {
      // Other runtime error
      if (mb==null) mb = new int[2];
      mb[0] = -1;
      mb[1] = -1;
      //throw new Exception(e.getMessage());
    }
    return mb;
  }
  
  /**
   * This method gets the first encountered outer matching brackets.<p>
   * 
   * It returns <tt>&lt;null&gt;</tt> when no brackets are found.
   * It returns [0]=-1, [1]=-1 when bracket sets are imbalance.
   * It returns [0]=-2, [1]=-2 for other runtime error.
   * 
   * Ignore.. For future use.
   * 
   * @param exp
   * @return
   * @throws Exception
   */
  private static int[] outerMatchingBrackets(String exp) throws Exception {
    int[] omb = null;
    try {
      int bO = 0;
      int bC = 0;
      int mb1 = -1;
      int mb2 = -1;
      char[] c = exp.toCharArray();
      for (int i=0; i<c.length; i++) {
        if (c[i]=='(') {
          bO++;
          if (mb1==-1) mb1 = i; // save first outer open bracket
        } else if (c[i]==')') {
          if (bC==0 && bO==0) {
            // first detection must be an open bracket, if not, exit loop
            break;
          }
          bC++;
          if (bO==bC) {
            if (mb2==-1) mb2 = i; // save matched close bracket
          }
        }
      }
      if (bO>0) {
        omb = new int[2];
        if (bO!=bC) {
          // bracket imbalance
          mb1 = -1;
          mb2 = -1;
        }
        omb[0] = mb1;
        omb[1] = mb2;
      }
    } catch (Exception e) {
      // Other runtime error
      if (omb==null) omb = new int[2];
      omb[0] = -2;
      omb[1] = -2;
    }
    return omb;
  }
  
  /**
   * This method checks for arithmetic expression rule:
   * Acceptable characters are 0 to 9 . * / + - ( ) space
   * @param exp
   * @return
   */
  public static boolean validateChars(String exp) {
    boolean valid = false;
    try {
      // Check valid characters 0 - 9 . * / + - ( ) space
      boolean validChars = Pattern.matches("[0-9\\.\\x20\\-\\+\\(\\)\\*/]+", exp);
      valid = validChars;
    } catch (Exception e) {
      valid = false;
    }
    return valid;
  }
  
  /**
   * This method checks if the brackets is balanced and that each open bracket
   * has a matching close bracket<p>
   * 
   * @param exp
   * @return
   */
  public static boolean validateBrackets(String exp) {
    boolean valid = false;
    try {
      int [] mb = innerMatchingBrackets(exp);
      if (mb==null || mb[0]!=-1 && mb[1]!=-1) {
        // bracket is balanced and matched
        valid = true;
      }
    } catch (Exception e) {
    }
    return valid;
  }
  
  /**
   * This method checks if the expression is valid based on the rules:<p>
   * <ul>
   * <li>The expression must begin with either a number, an open bracket,
   * a plus, a minus
   * <li>The expression must end with either a number or a close bracket.
   * <li>Valid next tokens, see table below.
   * <ul>
   * 
   * <table border="1" style="border-collapse:collapse">
   * <tr><td rowspan="2">current</td><td colspan="7">next token</td></tr>
   * <tr>
   * <td width="12%">(</td>
   * <td width="12%">+</td>
   * <td width="12%">-</td>
   * <td width="12%">*</td>
   * <td width="12%">/</td>
   * <td width="12%">N</td>
   * <td width="12%">)</td>
   * </tr>
   * <tr>
   * <td>(</td>
   * <td>1</td>
   * <td>1</td>
   * <td>1</td>
   * <td>0</td>
   * <td>0</td>
   * <td>1</td>
   * <td>0</td>
   * </tr>
   * <tr>
   * <td>)</td>
   * <td>0</td>
   * <td>1</td>
   * <td>1</td>
   * <td>1</td>
   * <td>1</td>
   * <td>0</td>
   * <td>1</td>
   * </tr>
   * <tr>
   * <td>+</td>
   * <td>1</td>
   * <td>0</td>
   * <td>0</td>
   * <td>0</td>
   * <td>0</td>
   * <td>1</td>
   * <td>0</td>
   * </tr>
   * <tr>
   * <td>*</td>
   * <td>1</td>
   * <td>0</td>
   * <td>0</td>
   * <td>0</td>
   * <td>0</td>
   * <td>1</td>
   * <td>0</td>
   * </tr>
   * <tr>
   * <td>/</td>
   * <td>1</td>
   * <td>0</td>
   * <td>0</td>
   * <td>0</td>
   * <td>0</td>
   * <td>1</td>
   * <td>0</td>
   * </tr>
   * <tr>
   * <td>N</td>
   * <td>0</td>
   * <td>1</td>
   * <td>1</td>
   * <td>1</td>
   * <td>1</td>
   * <td>0</td>
   * <td>1</td>
   * </tr>
   * </table>
   * 1 = valid <br>
   * 0 = invalid <br>
   * 
   * @param exp
   * @return
   */
  public static boolean validateTokens(String exp) {
    boolean valid = true;
    try {
      boolean firstTokenPass = false;
      boolean lastTokenPass = false;
      String firstToken = null;
      String lastToken = null;
      String prevToken = null;
      String [] tokens = exp.split(" ");
      
      for (String token : tokens) {
        if (token.trim().length()>0) {
          if (firstToken==null) firstToken = token;
          if (prevToken!=null) {
            if (prevToken.equals("(")) {
              if (!(token.equals("(")||token.equals("+")||token.equals("-")||isNumber(token))) {
                valid = false;
              }
            } else if (prevToken.equals(")")) {
              if (!(token.equals("+")||token.equals("-")||token.equals("*")||token.equals("/")||token.equals(")"))) {
                valid = false;
              }
            } else if (prevToken.equals("+")) {
              if (!(token.equals("(")||isNumber(token))) {
                valid = false;
              }
            } else if (prevToken.equals("-")) {
              if (!(token.equals("(")||isNumber(token))) {
                valid = false;
              }
            } else if (prevToken.equals("*")) {
              if (!(token.equals("(")||isNumber(token))) {
                valid = false;
              }
            } else if (prevToken.equals("/")) {
              if (!(token.equals("(")||isNumber(token))) {
                valid = false;
              }
            } else if (isNumber(prevToken)) {
              if (!(token.equals("+")||token.equals("-")||token.equals("*")||token.equals("/")||token.equals(")"))) {
                valid = false;
              }
            }
          }
          prevToken = token;
          lastToken = token;
        }
      }
      if (firstToken.equals("(")||firstToken.equals("+")||firstToken.equals("-")||isNumber(firstToken)) {
        firstTokenPass = true;
      }
      if (lastToken.equals(")")||isNumber(lastToken)) {
        lastTokenPass = true;
      }
      if (!firstTokenPass||!lastTokenPass) valid = false;
      
    } catch (Exception e) {
      valid = false;
    }
    return valid;
  }
  
  /**
   * Check if <tt>num</tt> is a number
   * @param num
   * @return
   */
  private static boolean isNumber(String num) {
    boolean valid = false;
    try {
      Double.parseDouble(num);
      valid = true;
    } catch (Exception e) {
    }
    return valid;
  }
  
  public static void main(String[] args) {
    try {
      
      Console console = System.console();
      
      System.out.println("********************************************************************");
      System.out.println("* Simple Expression Calculator v1.0.0b                             *");
      System.out.println("* Usage instructions:                                              *");
      System.out.println("* Key in a simple expression in a single line.                     *");
      System.out.println("* The numbers, brackets and operators must be separated by spaces. *");
      System.out.println("* For example, 3.5 * ( 2 - 5.41 * ( 1.01 + 3 ) )                   *");
      System.out.println("********************************************************************");
      
      
      do {
        String userExpression = console.readLine("Enter arithmetic expression <enter 'quit' to quit>:");
        
        if (userExpression.equals("quit")) break;

        try {
          double answer = calculate(userExpression);
          System.out.println("Answer: " + String.valueOf(answer));
        } catch (Exception e) {
          System.out.println("Expression invalid: " + e.getMessage());
        }
      
      } while (true);
      
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
    
  }
  
}
