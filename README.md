# excalc
---
#### Desciption
This is a simple arithmetic expression calculator. It is implemented as a runnable jar console app.
The app can be run by:
```
java -jar calc.jar
```
